import { Component } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { CardService } from './card.service';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html'
})
export class AppComponent {
	title = 'double';
	public isDeckSelectorVisible: boolean = false;

	constructor(
		public readonly cardService: CardService,
		private readonly router: Router
	) {
		this.checkRoute();
	}

	private checkRoute(): void {
		this.router.events.subscribe(e => {
			if(e instanceof NavigationEnd){
			  this.isDeckSelectorVisible = Boolean(e.url === '/deck');
			}
		  });
	}
}
