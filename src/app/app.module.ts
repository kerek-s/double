import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { AppRoutingModule } from 'src/app/routing/routing.module';
import { CardService } from 'src/app/card.service';
import { DeckComponent } from './deck/deck.component';
import { DeckSizeSelectorComponent } from './deck-size-selector/deck-size-selector.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';

@NgModule({
	declarations: [
		AppComponent,
		PageNotFoundComponent,
		DeckComponent,
		DeckSizeSelectorComponent
	],
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		AppRoutingModule,
		ReactiveFormsModule,
		ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
	],
	providers: [
		CardService
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
